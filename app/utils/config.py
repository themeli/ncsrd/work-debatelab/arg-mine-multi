import logging
from logging.config import dictConfig
from pathlib import Path
from os import mkdir, environ
from os.path import exists, join

import yaml
import torch


class AppConfig:

    def __init__(self):
        self._make_dirs()
        self.properties = self.load_properties()
        self.logger = self._config_logger()
        self._configure_device()

    def _make_dirs(self):
        self.app_path = str(Path(__file__).parent.parent)

        # input folders
        self.resources_path = join(self.app_path, "resources")
        self.data_path = join(self.resources_path, "data")

        # output folders
        self.output_path = join(self.app_path, "output")
        if not exists(self.output_path):
            mkdir(self.output_path)
        self.models_path = join(self.output_path, "models")
        if not exists(self.models_path):
            mkdir(self.models_path)
        self.adu_model_path = join(self.models_path, "adu")
        if not exists(self.adu_model_path):
            mkdir(self.adu_model_path)
        self.rel_model_path = join(self.models_path, "rel")
        if not exists(self.rel_model_path):
            mkdir(self.rel_model_path)
        self.stance_model_path = join(self.models_path, "stance")
        if not exists(self.stance_model_path):
            mkdir(self.stance_model_path)
        self.logs_path = join(self.output_path, "logs")
        if not exists(self.logs_path):
            mkdir(self.logs_path)

    def load_properties(self):
        """
        Function to load the application properties

        Returns
            | dict: the application properties
        """
        properties_file = "properties.yaml"
        path_to_properties = join(self.resources_path, properties_file)
        with open(path_to_properties, "r") as f:
            return yaml.safe_load(f.read())

    def _config_logger(self):
        """
        Configures the application logger

        Returns
            logger: the initialized logger
        """
        filename = f"logs.log"
        log_file_path = join(self.logs_path, filename)
        logging_level = self.properties.get("logging_level", "DEBUG")
        logging_config = {
            'version': 1,
            'disable_existing_loggers': True,
            'formatters': {
                'standard': {
                    'format': '%(asctime)s,%(msecs)d %(levelname)-1s [%(filename)s:%(lineno)d] %(message)s',
                    'datefmt': "%d/%b/%Y %H:%M:%S"
                },
            },
            'handlers': {
                'null': {
                    'level': logging_level,
                    'class': 'logging.NullHandler',
                },
                'logfile': {
                    'level': logging_level,
                    'class': 'logging.handlers.TimedRotatingFileHandler',
                    'filename': log_file_path,
                    'when': 'midnight',  # this specifies the interval
                    'interval': 1,  # defaults to 1, only necessary for other values
                    'backupCount': 30,  # how many backup file to keep, 10 days
                    'formatter': 'standard',
                },
                'console': {
                    'level': logging_level,
                    'class': 'logging.StreamHandler',
                    'formatter': 'standard'
                },
            },
            'loggers': {
                'am': {
                    'handlers': ['console', 'logfile'],
                    'level': logging_level,
                },
            }
        }
        dictConfig(logging_config)
        return logging.getLogger("am")

    def _configure_device(self):
        """
        Reads the environmental variable CUDA_VISIBLE_DEVICES in order to initialize the device to be used
        in the training
        """
        if torch.cuda.is_available():
            devices = environ.get("CUDA_VISIBLE_DEVICES", 0)
            if type(devices) == str:
                devices = devices.split(",")
                self.device_name = f"cuda:{devices[0].strip()}"
            else:
                self.device_name = f"cuda:{devices}"
        else:
            self.device_name = "cpu"
