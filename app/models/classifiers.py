import glob
import os
from os import listdir
from os.path import exists, join

import numpy as np
from sklearn.metrics import accuracy_score, f1_score
from transformers import XLMRobertaForTokenClassification, XLMRobertaForSequenceClassification, \
    DataCollatorForTokenClassification, DataCollatorWithPadding, Trainer, TrainingArguments, ProgressCallback, \
    PrinterCallback
from transformers.integrations import TensorBoardCallback
from transformers.trainer_utils import IntervalStrategy

from models.data import AMDataset
from utils.config import AppConfig


class ArgMiningTransformer:

    def __init__(self, app_config: AppConfig, dataset: AMDataset):
        self.app_config: AppConfig = app_config
        self.logger = app_config.logger
        # load tokenizer
        self.tokenizer = dataset.tokenizer
        self.task = dataset.task
        self.am_dataset = dataset
        self.device_name = app_config.device_name
        self.num_labels = dataset.num_labels

        # load model properties
        self.retrain = self.app_config.properties.get("retrain", False)
        self.model_properties = self.app_config.properties["model"]
        self.model_name = self.model_properties["name"]
        self.batch_size = self.model_properties["batch_size"]
        self.train_epochs = self.model_properties["epochs"]
        self.learning_rate = self.model_properties["learning_rate"]

        self.model_path = join(self.app_config.models_path, self.task, self.model_name)

    def train(self, resume=None):
        self.logger.info(f"Training model {self.model_name} for {self.task} task")

        if exists(self.model_path) and self.retrain:
            checkpoints = listdir(self.model_path)

            if not checkpoints:
                model, resume = self.get_model()
            else:
                list_of_files = glob.glob(f"{self.model_path}/*")  # * means all if need specific format then *.csv
                latest_file = max(list_of_files, key=os.path.getctime)
                self.logger.info(f"Loading model from checkpoint {latest_file}")
                model = XLMRobertaForTokenClassification.from_pretrained(latest_file, num_labels=self.num_labels) if \
                    self.task == "adu" else XLMRobertaForSequenceClassification(latest_file, num_labels=self.num_labels)
                model.to(self.device_name)
                resume = True if resume is None else resume
        else:
            model, resume = self.get_model()

        self.logger.info("Creating DataCollator")
        data_collator = DataCollatorForTokenClassification(tokenizer=self.tokenizer, padding=True,
                                                           max_length=self.am_dataset.seqlen) \
            if self.task == "adu" else DataCollatorWithPadding(tokenizer=self.tokenizer, padding=True,
                                                               max_length=self.am_dataset.seqlen)

        self.logger.info("Creating training args & trainer")
        training_args = self.get_training_args()
        trainer = self.get_trainer(data_collator=data_collator, model=model, training_args=training_args)

        self.logger.info("Start training")
        trainer.train(resume_from_checkpoint=resume)
        self.logger.info("Training finished")
        self.logger.info(f"Saving trained model to disk. Model's path: {self.model_path}")
        trainer.save_model(self.model_path)

    def get_model(self):
        model = XLMRobertaForTokenClassification.from_pretrained(self.model_name, num_labels=self.num_labels) if \
            self.task == "adu" else XLMRobertaForSequenceClassification.from_pretrained(self.model_name,
                                                                                        num_labels=self.num_labels)
        model.to(self.device_name)
        resume = None
        return model, resume

    def eval(self):
        # TODO pipelines
        pass

    def get_trainer(self, data_collator, model, training_args):
        return Trainer(
            tokenizer=self.tokenizer,
            model=model,  # the instantiated 🤗 Transformers model to be trained
            args=training_args,  # training arguments, defined above
            data_collator=data_collator,
            train_dataset=self.am_dataset.train_dataset,
            eval_dataset=self.am_dataset.test_dataset,
            callbacks=[TensorBoardCallback, ProgressCallback, PrinterCallback],
            compute_metrics=self.compute_metrics
        )

    def get_training_args(self):
        return TrainingArguments(
            output_dir=self.model_path,
            do_train=True,
            do_eval=True,
            evaluation_strategy=IntervalStrategy.STEPS,
            eval_steps=1000,
            load_best_model_at_end=True,
            num_train_epochs=self.train_epochs,
            per_device_train_batch_size=self.batch_size,  # batch size per device during training
            per_device_eval_batch_size=self.batch_size,
            learning_rate=self.learning_rate,
            report_to=["tensorboard"],
            metric_for_best_model="loss",
            logging_strategy=IntervalStrategy.STEPS,
            logging_first_step=True,
            save_strategy=IntervalStrategy.STEPS,
            save_steps=1000,
            save_total_limit=10,
            dataloader_num_workers=8,
            logging_dir=self.app_config.logs_path  # directory for storing logs
        )

    @staticmethod
    def compute_metrics(eval_pred):
        logits, labels = eval_pred
        predictions = np.argmax(logits, axis=-1)
        # flatten
        labels = np.reshape(labels, (-1,))
        predictions = np.reshape(predictions, (-1,))

        res = {"macro_f1": f1_score(labels, predictions, average="macro"),
               "micro_f1": f1_score(labels, predictions, average="micro"),
               "accuracy": accuracy_score(labels, predictions)}
        return res
