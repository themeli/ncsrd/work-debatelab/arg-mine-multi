from os.path import join
from typing import AnyStr

import numpy as np
import pandas as pd
import torch
from datasets import load_dataset, load_from_disk
from sklearn.preprocessing import LabelEncoder
from torch.utils.data import Dataset
from transformers import AutoTokenizer

from utils.config import AppConfig


class AMDataset:

    def __init__(self, app_config: AppConfig, task: AnyStr):
        self.app_config = app_config
        self.logger = app_config.logger
        self.model_name = self.app_config.properties["model"]["name"]
        self.tokenizer = AutoTokenizer.from_pretrained(self.model_name)
        self.task = task
        self.train_dataset, self.test_dataset = None, None
        self.seqlen = self.app_config.properties["seqlen"]
        self.num_labels = -1

    def load_data(self):
        raise NotImplementedError


class MyDataset(Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val).squeeze() for key, val in self.encodings[idx].items()}
        # item['label'] = torch.tensor(self.labels[idx])
        item['labels'] = self.labels[idx]
        # return {"input_ids": self.encodings[idx], "labels": self.labels[idx]}
        return item

    def __len__(self):
        return len(self.labels)


class SequentialDataset(AMDataset):

    def __init__(self, app_config: AppConfig, task: AnyStr):
        super(SequentialDataset, self).__init__(app_config=app_config, task=task)

    @staticmethod
    def read_data(folder_path):
        data = {}
        # read data
        for x in "train dev test".split():
            path = join(folder_path, x + ".csv")
            data[x] = pd.read_csv(path, header=None, sep="\t")[[0, 1]]
        # join train & dev
        data["train"] = data["train"].append(data["dev"])
        del data["dev"]

        for k, v in data.items():
            # split
            tokens, labels = v[0].values.tolist(), v[1].values
            nans = [i for i, x in enumerate(tokens) if x is np.nan]
            # add first and last indexes
            nans = [0] + nans + [-1]
            sentences = [tokens[nans[i] + 1:nans[i + 1]] for i in range(len(nans) - 1)]
            labels = [labels[nans[i] + 1:nans[i + 1]] for i in range(len(nans) - 1)]
            data[k] = (sentences, labels)
        return data["train"], data["test"]

    @staticmethod
    def pad_labels(labels, maxlen, dummy_label=-100):
        for i, label in enumerate(labels):
            label = label[:maxlen].tolist()
            if len(label) < maxlen:
                label += [dummy_label for _ in range(maxlen - len(label))]
            labels[i] = label
        if len(set(len(v) for v in labels)) != 1:
            raise ValueError("Label padding problematic -- different lengths encountered!")
        return labels

    @staticmethod
    def tokenize(data, tok, seqlen):
        return [tok(dat, is_split_into_words=True, return_tensors='pt', add_special_tokens=True, padding="max_length",
                    max_length=seqlen, truncation=True) for dat in data]

    def load_data(self):
        data_folder = join(self.app_config.data_path, "adu")
        train, test = self.read_data(data_folder)
        # train = train[0][:100], train[1][:100]
        # test = test[0][:100], test[1][:100]

        # tokenization
        self.logger.info(f"Tokenizing with {self.model_name}")
        self.logger.info(
            f"BOS {self.tokenizer.bos_token_id}, EOS {self.tokenizer.eos_token_id}, UNK {self.tokenizer.unk_token_id} "
            f"CLS {self.tokenizer.cls_token_id} "
            f"MASK {self.tokenizer.mask_token_id}")

        le = LabelEncoder()
        train_data = self.tokenize(train[0], self.tokenizer, self.seqlen)
        train_labels = train[1]
        all_train_labels = np.concatenate(train_labels)
        le.fit_transform(all_train_labels)
        train_labels = [le.transform(tl) for tl in train_labels]
        token_labels = np.unique(np.concatenate(train_labels))

        self.logger.info("Tokenizing test")
        test_data = self.tokenize(test[0], self.tokenizer, self.seqlen)
        test_labels = [le.transform(tl) for tl in test[1]]

        train_labels = self.pad_labels(train_labels, maxlen=self.seqlen)
        test_labels = self.pad_labels(test_labels, maxlen=self.seqlen)
        self.num_labels = len(token_labels)
        self.train_dataset = MyDataset(train_data, train_labels)
        self.test_dataset = MyDataset(test_data, test_labels)
        return self.train_dataset, self.test_dataset


class TextClassificationDataset(AMDataset):

    def __init__(self, app_config: AppConfig, task: AnyStr):
        super(TextClassificationDataset, self).__init__(app_config=app_config, task=task)
        self.cache_dir = join(self.app_config.data_path, task, "cache")
        self.tokenized_folder = join(self.app_config.data_path, task, "tokenized")

    def load_data(self):
        try:
            self.logger.info("Loading train and test datasets from disk")
            train_path = join(self.tokenized_folder, "train")
            test_path = join(self.tokenized_folder, "test")
            self.train_dataset = load_from_disk(train_path)
            self.test_dataset = load_from_disk(test_path)
        except(BaseException, Exception) as e:
            self.logger.error(e)
            self.train_dataset, self.test_dataset = self.create_datasets()
        self.num_labels = len(list(set(self.train_dataset["labels"])))
        return self.train_dataset, self.test_dataset

    def create_datasets(self):
        self.logger.info("Creating Train and Test Datasets")

        # initialize path to raw datasets
        train_path = join(self.app_config.data_path, self.task, "train.csv")
        test_path = join(self.app_config.data_path, self.task, "test.csv")

        # load raw datasets from disk
        self.logger.info("Loading train and test datasets from given paths")
        datasets = load_dataset('csv', data_files={'train': train_path, 'test': test_path}, skiprows=1,
                                cache_dir=self.cache_dir, column_names=["sentences", "labels"], delimiter="\t")
        train_dataset, test_dataset = datasets["train"], datasets["test"]

        self.num_labels = len(list(set(train_dataset["labels"])))

        # create tokenized dataset
        train_dataset = train_dataset.map(
            lambda e: self.tokenizer(e['sentences'], truncation=True, padding='max_length'),
            batched=True)
        test_dataset = test_dataset.map(lambda e: self.tokenizer(e['sentences'], truncation=True, padding='max_length'),
                                        batched=True)
        train_dataset.set_format(type='torch', columns=['input_ids', 'attention_mask', 'labels'])
        test_dataset.set_format(type='torch', columns=['input_ids', 'attention_mask', 'labels'])

        train_dataset.save_to_disk(join(self.tokenized_folder, "train"))
        test_dataset.save_to_disk(join(self.tokenized_folder, "test"))
        return train_dataset, test_dataset
