from os.path import join
import pandas as pd
from models.data import SequentialDataset, TextClassificationDataset
from models.classifiers import ArgMiningTransformer
from utils.config import AppConfig


def preprocess(app_config: AppConfig):
    task = app_config.properties["task"]
    data_folder = join(app_config.data_path, task)
    train_data = join(data_folder, "train.csv")
    test_data = join(data_folder, "test.csv")
    convert_data(data_path=train_data)
    convert_data(data_path=test_data)


def convert_data(data_path):
    df = pd.read_csv(data_path, sep="\t", index_col=None, header=None)
    df_new = pd.DataFrame(columns=["sentences", "labels"])
    df_new["sentences"] = df[0]
    df_new["labels"] = df[1]
    df_new.to_csv(data_path, sep="\t", index=False, header=True)


def load_dataset(app_config: AppConfig):
    task = app_config.properties["task"]
    dataset = SequentialDataset(app_config=app_config, task=task) if task == "adu" else \
        TextClassificationDataset(app_config=app_config, task=task)
    dataset.load_data()
    return dataset


def load_model(app_config, dataset):
    classifier = ArgMiningTransformer(app_config=app_config, dataset=dataset)
    classifier.train()


def train():
    pass


def evaluate():
    pass


def main():
    app_config = AppConfig()
    dataset = load_dataset(app_config=app_config)
    load_model(app_config=app_config, dataset=dataset)


if __name__ == '__main__':
    main()
